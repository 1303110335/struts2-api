<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/9 0009
  Time: 18:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>$Title$</title>
  </head>
  <body>
  <%--<a href="${pageContext.request.contextPath}/p1/demo1.action">demo1测试转发跳转</a>
  <a href="${pageContext.request.contextPath}/p1/demo2.action">demo2测试转发跳转</a>--%>

  <%--请求参数封装：第一种情况：属性驱动-没有实体类--%>
  <%--<form action="${pageContext.request.contextPath}/test/p1/demo3.action" method="post">
      姓名：<input type="text" name="username"/><br/>
      年龄：<input type="text" name="age"/><br/>
      生日：<input type="text" name="birthday"/><br/>
      爱好：<input type="checkbox" name="hobby" value="吃饭"/><br/>
            <input type="checkbox" name="hobby" value="睡觉"/>
            <input type="checkbox" name="hobby" value="写代码"/>
            <input type="submit" value="提交">
    <br/>
  </form>--%>


  <%--请求参数封装：第二种情况：属性驱动-有实体类--%>
  <%--<form action="${pageContext.request.contextPath}/test/p1/demo4.action" method="post">
    姓名：<input type="text" name="user.username"/><br/>
    年龄：<input type="text" name="user.age"/><br/>
    生日：<input type="text" name="user.birthday"/><br/>
    爱好：<input type="checkbox" name="user.hobby" value="吃饭"/><br/>
    <input type="checkbox" name="user.hobby" value="睡觉"/>
    <input type="checkbox" name="user.hobby" value="写代码"/>
    <input type="submit" value="提交">
    <br/>
  </form>
--%>

  <%--请求参数封装：第三种情况：模型驱动--%>
  <%--<form action="${pageContext.request.contextPath}/test/p1/demo5.action" method="post">
    姓名：<input type="text" name="username"/><br/>
    年龄：<input type="text" name="age"/><br/>
    生日：<input type="text" name="birthday"/><br/>
    爱好：<input type="checkbox" name="hobby" value="吃饭"/>
    <input type="checkbox" name="hobby" value="睡觉"/>
    <input type="checkbox" name="hobby" value="写代码"/><br/>
    <input type="submit" value="提交">
    <br/>
  </form>--%>

  <%--请求参数封装：list集合类型的封装--%>
  <form action="${pageContext.request.contextPath}/test/p1/demo6.action" method="post">
    姓名：<input type="text" name="users[0].username"/><br/>
    年龄：<input type="text" name="users[0].age"/><br/>
    生日：<input type="text" name="users[0].birthday"/><br/>
    爱好：<input type="checkbox" name="users[0].hobby" value="吃饭"/>
    <input type="checkbox" name="users[0].hobby" value="睡觉"/>
    <input type="checkbox" name="users[0].hobby" value="写代码"/><br/>
    <br/>

    姓名：<input type="text" name="users[1].username"/><br/>
    年龄：<input type="text" name="users[1].age"/><br/>
    生日：<input type="text" name="users[1].birthday"/><br/>
    爱好：<input type="checkbox" name="users[1].hobby" value="吃饭"/>
    <input type="checkbox" name="users[1].hobby" value="睡觉"/>
    <input type="checkbox" name="users[1].hobby" value="写代码"/><br/>
    <br/>

    姓名：<input type="text" name="users[2].username"/><br/>
    年龄：<input type="text" name="users[2].age"/><br/>
    生日：<input type="text" name="users[2].birthday"/><br/>
    爱好：<input type="checkbox" name="users[2].hobby" value="吃饭"/>
    <input type="checkbox" name="users[2].hobby" value="睡觉"/>
    <input type="checkbox" name="users[2].hobby" value="写代码"/><br/>
    <input type="submit" value="提交">
    <br/>
  </form>
  </body>
</html>
