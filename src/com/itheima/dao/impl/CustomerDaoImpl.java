package com.itheima.dao.impl;

import com.itheima.dao.ICustomerDao;
import com.itheima.domain.Customer;
import com.itheima.utils.HibernateUtils;
import org.hibernate.query.Query;

import java.util.List;

public class CustomerDaoImpl implements ICustomerDao {
    @Override
    public List<Customer> findAllCustomer() {
        return HibernateUtils.getCurrentSession().createQuery("from Customer ").list();
    }
}
