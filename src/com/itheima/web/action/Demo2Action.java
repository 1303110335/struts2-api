package com.itheima.web.action;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.util.ServletContextAware;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 访问servlet api
 * 有两种方式：
 *      第二种方式：
 *          通过实现不同的接口，获取不同的对象
 *          要想使用request,需要实现ServletRequestAware
 *          要想使用response,需要实现ServletResponseAware
 *          要想使用servletContext,需要实现ServletContextAware
 *
 * 输出结果之后，找出其中一个和其他三个不一样：
 * org.apache.struts2.dispatcher.StrutsRequestWrapper@4b19294e 它和其他三个不一样，是struts2提供的
 * org.apache.catalina.connector.ResponseFacade@470cf88c
 * org.apache.catalina.core.ApplicationContextFacade@3cb7c2a3
 * org.apache.catalina.session.StandardSessionFacade@1c6734b7
 *
 *  如果说是一种方式获取ServletAPI对象：ActionContext中的get(key)
 *  如果说是三种方式获取ServletAPI对象，除了我们将的两种之外，也可以使用ActionContext获取
 *
 *  通过源码分析，我们得知，ActionContext看上去是一个类似map的结构
 *  map的key是String类型，Map的value是Object类型
 */
public class Demo2Action extends ActionSupport implements ServletRequestAware, ServletResponseAware, ServletContextAware {

    private HttpServletRequest request = null;

    private HttpServletResponse response = null;

//    private HttpSession session;

    private ServletContext application = null;
    /**
     * @return
     */
    public String demo2() {


        System.out.println(request);
        System.out.println(response);
        System.out.println(application);
//        System.out.println(session);
        return SUCCESS;
    }

    @Override
    public void setServletRequest(HttpServletRequest httpServletRequest) {
        this.request = httpServletRequest;
    }

    @Override
    public void setServletResponse(HttpServletResponse httpServletResponse) {
        this.response = httpServletResponse;
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.application = servletContext;
    }
}
