package com.itheima.web.action;

import com.itheima.domain.User;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

/**
 * 请求参数封装
 * 第三种情况：
 *     模型驱动
 * 要想封装成功，要求：
 *  1.动作类必须实现ModelDriven接口
 *  2.动作类中需要定义模型，并且必须实例化出来
 *  3.提供接口抽象方法的实现，返回值必须是模型对象
 *
 *
 *
 * 执行参数封装，是一个名称为params的拦截器实现的
 *      模型驱动的实现，除了params拦截器外，还需要一个叫modelDriven的拦截器
 *      封装的规则只有一个，它要去指定的位置找属性，找到之后调用set方法赋值
 *
 *
 */
public class Demo5Action extends ActionSupport implements ModelDriven<User> {

    private User user = new User();

    /**
     * 动作方法
     * @return
     */
    public String demo5() {
        System.out.println(user);
        return SUCCESS;
    }

    @Override
    public User getModel() {
        return user;
    }
}
