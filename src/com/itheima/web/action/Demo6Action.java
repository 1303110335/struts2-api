package com.itheima.web.action;

import com.itheima.domain.User;
import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * 请求参数封装
 * 第四种情况：
 *      复杂类型的封装：List类型的封装
 *      复杂类型的封装都需要基于第二种情况实现
 *
 */
public class Demo6Action extends ActionSupport{

    private List<User> users = new ArrayList<>();

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    /**
     * 动作方法
     * @return
     */
    public String demo6() {
        System.out.println(users);
        return SUCCESS;
    }
}
