package com.itheima.web.action;

import com.opensymphony.xwork2.ActionSupport;

import java.util.Date;

/**
 * 请求参数封装
 * 第一种情况：
 *      属性驱动：没有实体类
 *      表单数据的接收都定义在动作类中，所以也称为动作类和模型数据写在一起
 * 要想封装陈宫，需要按照要求书写
 *      要求是：表单中name属性取值，必须和动作类中成员get/set方法后面的部分保持一致
 *
 * 细节：
 *      1、struts2框架在解决了post请求的中文乱码问题，但是get没有解决
 *      2、struts2框架会自动为我们装换数据类型
 *              基本类型转换
 *              字符串数组会按照逗号+空格的方式拼接成字符串
 *              日期类型会按照本地格式转成日期对象
 *                  本地格式：yyyy-MM-dd
 *
 * 执行参数封装，是一个名称为params的拦截器实现的
 *      封装的规则只有一个，它要去指定的位置找属性，找到之后调用set方法赋值
 *
 *
 */
public class Demo3Action extends ActionSupport {

    private String username;
    private Integer age;
    private Date birthday;
    private String hobby;


    /**
     * 动作方法
     * @return
     */
    public String demo3() {
        System.out.println(username);
        System.out.println(this);
        return SUCCESS;
    }


    @Override
    public String toString() {
        return "Demo3Action{" +
                "username='" + username + '\'' +
                ", age=" + age +
                ", birthday=" + birthday +
                ", hobby='" + hobby + '\'' +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }
}
