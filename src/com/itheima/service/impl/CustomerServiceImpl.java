package com.itheima.service.impl;

import com.itheima.dao.ICustomerDao;
import com.itheima.dao.impl.CustomerDaoImpl;
import com.itheima.domain.Customer;
import com.itheima.service.ICustomerService;
import com.itheima.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

/**
 * 客户的业务层实现
 */
public class CustomerServiceImpl implements ICustomerService {

    private ICustomerDao customerDao = new CustomerDaoImpl();

    @Override
    public List<Customer> findAllCustomer() {
        Session s = null;
        Transaction tx = null;
        try {
            s = HibernateUtils.getCurrentSession();
            tx = s.beginTransaction();
            List<Customer> customers = customerDao.findAllCustomer();
            tx.commit();
            //提交事务，并返回结果
            return customers;
        } catch (Exception e) {
            //失败回滚
            tx.rollback();
            throw new RuntimeException(e);
        }
    }
}
