package com.itheima.service.impl;

import com.itheima.domain.Customer;
import com.itheima.service.ICustomerService;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * 测试用户的业务层和持久层
 */
public class CustomerServiceImplTest {

    @Test
    public void findAllCustomer() {
        ICustomerService customerService = new CustomerServiceImpl();
        List<Customer> customers = customerService.findAllCustomer();
        for (Customer c : customers) {
            System.out.println(c);
        }
    }
}
