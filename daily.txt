Struts2四天课
第一天：
    Struts2入门
    能够独立搭建struts2的开发环境
    能够使用struts2+hibernate实现客户列表的查询

第二天：
    Struts2中的请求参数封装
    Struts2中使用原始ServletAPI对象的方式
        原始ServletAPI对象：
            HttpServletRequest
            HttpServletResponse
            HttpSession
            ServletContext

    struts2提供的请求参数封装

第三天：
    OGNL表达式和OGNL上下文
    能够使用OGNL表达式封装数据
    能够明白OGNL上下文中分装着什么数据，是什么结构

第四天：
    struts2中自定义拦截器和基于注解的配置方式
    能够编写字段一拦截器
    能够使用注解配置struts2

1.三层架构
    表现层：接收和处理请求的
        MVC模型：（表现层模型）
    业务层：处理程序业务需求的
    持久层：对数据库操作
2.MVC模型
    M:model         模型      作用：封装数据     目前就是实体类作为模型
    V:view          视图      作用：展示数据     JSP/HTML
    C:controller    控制器     作用：处理数据（控制程序流转）Servlet/Filter

3.Servlet 和 Filter
    共同点：
        都有3个常用方法：初始化，销毁和核心方法(service, doFilter)
        他们的核心方法都有request和response
        他们都是单例对象，即一个应用只有一个对象

    区别：
        Servlet ： 请求第一次到达时（默认情况下），资源
        Filter : 应用已加载就创建

4.在线商城案例中的小问题：
    访问时Servlet中有很多的if和else判断
    我们解决判断过的方式：BaseServlet
    BaseServlet中有个问题：jsp中访问路径的method取值和Servlet的方法名称绑定了
    解决方案：使用配置文件

struts2中的6个配置文件：（按顺序加载）
    default.properties
    struts-default.xml
    struts-plugin.xml
    struts.xml
    struts.properties
    web.xml




1.result标签
    作用及属性
    结果试图分类（全局结果视图/局部结果视图）
2.访问ServletAPI的两种方式
    ServletAPI:
        HttpServletRequest
        HttpServletResponse
        HttpSession
        ServletContext
3.Struts2中的请求参数封装
    三种封装方式：
        属性驱动-没有实体类
        属性驱动-有实体类
        模型驱动

    复杂数据类型的封装（集合类型List/Map)
4.案例：使用struts2实现用户的保存

5.请求参数封装失败后的处理办法






























